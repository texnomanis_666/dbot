package game

import (
	"dbot/utils"
	"math/rand"
	"time"
)

type WordsArrs struct {
	Arr_a   []string
	Arr_b   []string
	Arr_v   []string
	Arr_g   []string
	Arr_d   []string
	Arr_zh  []string
	Arr_e   []string
	Arr_yo  []string
	Arr_z   []string
	Arr_i   []string
	Arr_ii  []string
	Arr_k   []string
	Arr_l   []string
	Arr_m   []string
	Arr_n   []string
	Arr_o   []string
	Arr_p   []string
	Arr_r   []string
	Arr_s   []string
	Arr_t   []string
	Arr_y   []string
	Arr_f   []string
	Arr_x   []string
	Arr_c   []string
	Arr_ch  []string
	Arr_sh  []string
	Arr_sch []string
	Arr_ie  []string
	Arr_u   []string
	Arr_ya  []string
}

var WA WordsArrs

func InitGame() {

	WA = WordsArrs{
		Arr_a:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_а.txt"),
		Arr_b:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_б.txt"),
		Arr_v:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_в.txt"),
		Arr_g:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_г.txt"),
		Arr_d:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_д.txt"),
		Arr_zh:  utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_ж.txt"),
		Arr_e:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_е.txt"),
		Arr_yo:  utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_ё.txt"),
		Arr_z:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_з.txt"),
		Arr_i:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_и.txt"),
		Arr_ii:  utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_й.txt"),
		Arr_k:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_к.txt"),
		Arr_l:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_л.txt"),
		Arr_m:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_м.txt"),
		Arr_n:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_н.txt"),
		Arr_o:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_о.txt"),
		Arr_p:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_п.txt"),
		Arr_r:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_р.txt"),
		Arr_s:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_с.txt"),
		Arr_t:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_т.txt"),
		Arr_y:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_у.txt"),
		Arr_f:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_ф.txt"),
		Arr_x:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_х.txt"),
		Arr_c:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_ц.txt"),
		Arr_ch:  utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_ч.txt"),
		Arr_sh:  utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_ш.txt"),
		Arr_sch: utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_щ.txt"),
		Arr_ie:  utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_э.txt"),
		Arr_u:   utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_ю.txt"),
		Arr_ya:  utils.LoadFileToArr(".\\game\\words_lists\\russian.txt_я.txt"),
	}

}

func GamePlay(wa *WordsArrs, last_simbol string) string {
	var s_arr []byte
	s_arr = []byte(last_simbol)

	// init radomize
	rand.Seed(time.Now().UnixNano())

	// а
	if s_arr[0] == 208 && s_arr[1] == 176 {
		return wa.Arr_a[rand.Intn(len(wa.Arr_a))]
	}

	//б
	if s_arr[0] == 208 && s_arr[1] == 177 {
		return wa.Arr_b[rand.Intn(len(wa.Arr_b))]
	}

	//в
	if s_arr[0] == 208 && s_arr[1] == 178 {
		return wa.Arr_v[rand.Intn(len(wa.Arr_v))]
	}

	//г
	if s_arr[0] == 208 && s_arr[1] == 179 {
		return wa.Arr_g[rand.Intn(len(wa.Arr_g))]
	}

	//д
	if s_arr[0] == 208 && s_arr[1] == 180 {
		return wa.Arr_d[rand.Intn(len(wa.Arr_d))]
	}

	//ж
	if s_arr[0] == 208 && s_arr[1] == 181 {
		return wa.Arr_zh[rand.Intn(len(wa.Arr_zh))]
	}

	//и
	if s_arr[0] == 208 && s_arr[1] == 184 {
		return wa.Arr_i[rand.Intn(len(wa.Arr_i))]
	}

	//й
	if s_arr[0] == 208 && s_arr[1] == 185 {
		return wa.Arr_ii[rand.Intn(len(wa.Arr_ii))]
	}

	//з
	if s_arr[0] == 208 && s_arr[1] == 183 {
		return wa.Arr_z[rand.Intn(len(wa.Arr_z))]
	}

	//к
	if s_arr[0] == 208 && s_arr[1] == 186 {
		return wa.Arr_k[rand.Intn(len(wa.Arr_k))]
	}

	//л
	if s_arr[0] == 208 && s_arr[1] == 187 {
		return wa.Arr_l[rand.Intn(len(wa.Arr_l))]
	}

	//м
	if s_arr[0] == 208 && s_arr[1] == 188 {
		return wa.Arr_m[rand.Intn(len(wa.Arr_m))]
	}

	//н
	if s_arr[0] == 208 && s_arr[1] == 189 {
		return wa.Arr_n[rand.Intn(len(wa.Arr_n))]
	}

	//о
	if s_arr[0] == 208 && s_arr[1] == 190 {
		return wa.Arr_o[rand.Intn(len(wa.Arr_o))]
	}

	//п
	if s_arr[0] == 208 && s_arr[1] == 191 {
		return wa.Arr_p[rand.Intn(len(wa.Arr_p))]
	}

	//р
	if s_arr[0] == 209 && s_arr[1] == 128 {
		return wa.Arr_r[rand.Intn(len(wa.Arr_r))]
	}

	//с
	if s_arr[0] == 209 && s_arr[1] == 129 {
		return wa.Arr_s[rand.Intn(len(wa.Arr_s))]
	}

	//т
	if s_arr[0] == 209 && s_arr[1] == 130 {
		return wa.Arr_t[rand.Intn(len(wa.Arr_t))]
	}

	//у
	if s_arr[0] == 209 && s_arr[1] == 131 {
		return wa.Arr_y[rand.Intn(len(wa.Arr_y))]
	}

	//е
	if s_arr[0] == 208 && s_arr[1] == 181 {
		return wa.Arr_e[rand.Intn(len(wa.Arr_e))]
	}

	//ё
	if s_arr[0] == 209 && s_arr[1] == 145 {
		return wa.Arr_yo[rand.Intn(len(wa.Arr_yo))]
	}

	//ф
	if s_arr[0] == 209 && s_arr[1] == 132 {
		return wa.Arr_f[rand.Intn(len(wa.Arr_f))]
	}

	//х
	if s_arr[0] == 209 && s_arr[1] == 133 {
		return wa.Arr_x[rand.Intn(len(wa.Arr_x))]
	}

	//ц
	if s_arr[0] == 209 && s_arr[1] == 134 {
		return wa.Arr_c[rand.Intn(len(wa.Arr_c))]
	}

	//ч
	if s_arr[0] == 209 && s_arr[1] == 135 {
		return wa.Arr_ch[rand.Intn(len(wa.Arr_ch))]
	}

	//ш
	if s_arr[0] == 209 && s_arr[1] == 136 {
		return wa.Arr_sh[rand.Intn(len(wa.Arr_sh))]
	}

	//щ
	if s_arr[0] == 209 && s_arr[1] == 137 {
		return wa.Arr_sch[rand.Intn(len(wa.Arr_sch))]
	}

	//э
	if s_arr[0] == 209 && s_arr[1] == 141 {
		return wa.Arr_ie[rand.Intn(len(wa.Arr_ie))]
	}

	//ю
	if s_arr[0] == 209 && s_arr[1] == 142 {
		return wa.Arr_u[rand.Intn(len(wa.Arr_u))]
	}

	//я
	if s_arr[0] == 209 && s_arr[1] == 143 {
		return wa.Arr_ya[rand.Intn(len(wa.Arr_ya))]
	}

	//ы
	if s_arr[0] == 209 && s_arr[1] == 139 {
		return "Ты победил. Я не знаю слов на букву Ы. Твой ход"
	}

	//ъ
	if s_arr[0] == 209 && s_arr[1] == 138 {
		return "Ты победил. Я не знаю слов на букву Ъ. Твой ход"
	}

	//ь
	if s_arr[0] == 209 && s_arr[1] == 140 {
		return "Ты победил. Я не знаю слов на букву Ь. Твой ход"
	}

	return "Ты победил"
}
