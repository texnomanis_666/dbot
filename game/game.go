package game

import (
	"dbot/utils"

	"github.com/bwmarrin/discordgo"
	//"golang.org/x/text/encoding/charmap"
)

var IsGame bool

var PlayerChannels []string

func Game(s *discordgo.Session, m *discordgo.MessageCreate) {
	s.ChannelMessageSend(m.ChannelID,
		"Вы начали игру в слова.\n"+
			"Вы придумываете слово, а я придумаю новое слово, которое заканчивается на последнюю букву вашего слова. Потом Вы придумаете слово по тому же приницпу. И так до бесконечности.\n"+
			"напишите -e или -exit чтобы выйти из игры")

	if !utils.CheckStringExists(PlayerChannels, m.ChannelID) {
		PlayerChannels = append(PlayerChannels, m.ChannelID)
	}
}

func Play(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID || !utils.CheckStringExists(PlayerChannels, m.ChannelID) {
		return
	}
	if m.Content == "-e" || m.Content == "-exit" {
		PlayerChannels = utils.RemoveString(PlayerChannels, m.ChannelID)
		s.ChannelMessageSend(m.ChannelID, "Пока. Поиграем в другой раз.")
	} else {
		s.ChannelMessageSend(m.ChannelID, GamePlay(&WA, m.Content[len(m.Content)-2:len(m.Content)]))
	}
}
