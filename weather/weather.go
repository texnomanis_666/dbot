package weather

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
)

type Coord struct {
	Lon float64 `json:"lon"`
	Lat float64 `json:"lat"`
}

type Weather struct {
	ID          int    `json:"id"`
	Main        string `json:"main"`
	Description string `json:"description"`
	Icon        string `json:"icon"`
}

type Main struct {
	Temp      float64 `json:"temp"`
	FeelsLike float64 `json:"feels_like"`
	TempMin   float64 `json:"temp_min"`
	TempMax   float64 `json:"temp_max"`
	Pressure  int     `json:"pressure"`
	Humidity  int     `json:"humidity"`
	SeaLevel  int     `json:"sea_level"`
	GrndLevel int     `json:"grnd_level"`
}

type Wind struct {
	Speed float64 `json:"speed"`
	Deg   int     `json:"deg"`
	Gust  float64 `json:"gust"`
}

type Rain struct {
	OneHour float64 `json:"1h"`
}

type Clouds struct {
	All int `json:"all"`
}

type Sys struct {
	Type    int    `json:"type"`
	ID      int    `json:"id"`
	Country string `json:"country"`
	Sunrise int    `json:"sunrise"`
	Sunset  int    `json:"sunset"`
}

type WeatherData struct {
	Coord      Coord     `json:"coord"`
	Weather    []Weather `json:"weather"`
	Base       string    `json:"base"`
	Main       Main      `json:"main"`
	Visibility int       `json:"visibility"`
	Wind       Wind      `json:"wind"`
	Rain       Rain      `json:"rain"`
	Clouds     Clouds    `json:"clouds"`
	DT         int       `json:"dt"`
	Sys        Sys       `json:"sys"`
	Timezone   int       `json:"timezone"`
	ID         int       `json:"id"`
	Name       string    `json:"name"`
	Cod        int       `json:"cod"`
}

func GetWeather(s *discordgo.Session, m *discordgo.MessageCreate) {

	city := "Almaty"

	apiKey := os.Getenv("WEATHER_TOKEN")
	units := "metric"

	words := strings.Split(m.Content, " ")

	if len(words) <= 1 {
		city = "Almaty"

	} else {
		city = words[1]
	}

	if city == "" {
		city = "Almaty"
	}

	url := fmt.Sprintf("http://api.openweathermap.org/data/2.5/weather?q=%s&units=%s&appid=%s", city, units, apiKey)

	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("Произошла ошибка при выполнении запроса.")
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Произошла ошибка при чтении ответа.")
		return
	}

	var weatherData WeatherData
	err = json.Unmarshal(body, &weatherData)
	if err != nil {
		fmt.Println("Произошла ошибка при разборе данных.")
		return
	}

	temperature := strconv.FormatFloat(weatherData.Main.Temp, 'f', 2, 64)
	humidity := strconv.Itoa(weatherData.Main.Humidity)
	pressure := strconv.Itoa(weatherData.Main.Pressure)
	wind := strconv.FormatFloat(weatherData.Wind.Speed, 'f', 2, 64)
	s.ChannelMessageSend(m.ChannelID, "Погода в городе : "+city+"\n"+
		"Температура : "+temperature+"\n"+
		"Влажность : "+humidity+"\n"+
		"Давление : "+pressure+"\n"+
		"Скорость ветра : "+wind)
}
