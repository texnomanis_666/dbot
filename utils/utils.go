package utils

import (
	"fmt"
	"io/ioutil"
	"strings"
)

// проверить наличие строки в массиве строк
func CheckStringExists(arr []string, searchString string) bool {
	for _, str := range arr {
		if str == searchString {
			return true
		}
	}
	return false
}

// удалить строку из массива
func RemoveString(arr []string, removeString string) []string {
	// Создаем новый пустой массив для хранения результата
	result := make([]string, 0)

	// Проходим по каждой строке в исходном массиве
	for _, str := range arr {
		// Если текущая строка не равна строке, которую нужно удалить, добавляем ее в новый массив
		if str != removeString {
			result = append(result, str)
		}
	}
	return result
}

// загрузить текстовый файл в масив строк
func LoadFileToArr(filename string) []string {
	data, err := ioutil.ReadFile(filename)
	// Если во время считывания файла произошла ошибка
	// выводим ее
	if err != nil {
		fmt.Println(err)
	}

	return strings.Split(string(data), string(byte(10)))

}

// this functin return command if string have bot prefix
func Prefix(prefix, string string) (string, bool) {
	result := ""
	if strings.HasPrefix(string, prefix) {

		if len(string) > len(prefix) && string[:len(prefix)] == prefix {
			result = string[len(prefix):]
			return result, true
		}

	}
	return result, false
}
