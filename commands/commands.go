package commands

import (
	"dbot/game"
	"dbot/utils"
	"dbot/weather"
	"os"
	"strings"

	"github.com/bwmarrin/discordgo"
)

func Help(s *discordgo.Session, m *discordgo.MessageCreate) {
	s.ChannelMessageSend(m.ChannelID,
		"-help or -h Help on bot commands (this message)\n"+
			"-game or -g Run a words game\n"+
			"-weather or -w Show a weather")
}

func CommandListener(s *discordgo.Session, m *discordgo.MessageCreate) {

	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}

	command, isComnand := utils.Prefix(os.Getenv("PREFIX"), m.Content)

	if isComnand {
		command = strings.ToLower(command)
		commandArr := strings.Split(command, " ")

		if len(commandArr) <= 1 {
			commandArr[0] = command
		}

		switch commandArr[0] {
		case "help":
		case "h":
			go Help(s, m)

		case "game":
		case "g":
			go game.Game(s, m)

		case "weather":
		case "w":
			go weather.GetWeather(s, m)

		default:
			s.ChannelMessageSend(m.ChannelID, "Sorry. This a not command.")
		}
	}

}
